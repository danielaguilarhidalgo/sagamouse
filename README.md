**Code used in the paper:**

## Predicting genetic interactions by inference of cis-regulatory elements dynamics in the early mouse eye development. ##

Daniel Aguilar-Hidalgo*, M. Carmen Lemos, María Tavares, Fernando Casares, Antonio Córdoba. In preparation. 2015.

*Corresponding authorship

    Code repository
    Version 1.0

**Who do I talk to?**

    Daniel Aguilar-Hidalgo: d.h.aguilar [at] gmail [dot] com

**Codes**

SAGA_SA.f: Simulated annealing code in FORTRAN. This code provides the initial population for the genetic algorithm in 'SAGA_GA'.

SAGA_GA.pl: Genetic algorithm code in PERL. This code optimizes the evolutionary dynamics in the transition between two networks, according to 4 possible logical dynamical rules, that retrieves the closest final network to a control one. The selected dynamics gives a small number of link changes, which are of great importance to commit with the selected dynamics.