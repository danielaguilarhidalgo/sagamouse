#!/usr/bin/perl

use Switch;

my $num_nodes;      #number of nodes
my @e9_nodes;       #array containing the nodes in the first network
my @e10_nodes;      #array containing the nodes in the second network
my @nodes_fit;      #array containing fitness_nodes scores
my $e9start;        #array position where first network nodes begin
my $e10start;       #array position where second network nodes begin
my @total_fit;      #array containing total fitness score considering the two fit functions
my @int_fit;        #array containing link scores
my $num_elem;       #number of elements in an individual
my $min_fit = 0.175;#fitness threshold
my @population;     #population of individuals to be evaluated
my @new_population; #population of the best individuals
my $num_ind;        #number of individuals in a population
my %saved_ind;      #positions and scores for individuals for reproduction
my @sorted;         #saved elements sorted by fitness
my $rand_num;       #random number between 0 and 1
my $rand_num2;      #random number between 0 and 1
my $num_states;     #number of evaluation states
my $num_edges;      #number of links
my $mut_rate_i = 0.0001;#mutation rate
my $mut_rate = 0.0001;#mutation rate
my $mut_rate2 = 0.5;mutation rate for links
my $init_mut_rate2 = 0.1;mutation rate for rules
my $aref;           #array reference
my $output;         #reference to output file
my $p;
my $q;
my $num_loop = 400000;  #maximum number of loops
my $top = 105; #maximum number of conexions
my $win_value = 0;	#initializing winner score parameter
my $lambda = 0.90;	#weight between node and link terms in the fitness function (set to 90% to nodes)
my $win_value2 = 0;	#initializing winner score parameter
my $sin_new = 0;	#initializing number generation without finding a better candidate
my $sin_new2 = 0;	#initializing number of last winner

&file_handler;
print "\nstarting initial generation\n";
&init_SA;
print OUTPUT "n\tPhi\tPhi2\n";
for($loop=1;$loop<=$num_loop;++$loop){
$win_value = 0;
$sin_new++;
$sin_new2++;
if($sin_new2 == 100000){
&solution;
exit;
}

if($sin_new == 2000){
  
  $mut_rate = $mut_rate+0.0001; #mutation rate is increased if  $sin_new generations without better candidate
  $sin_new = 0;
}
if($mut_rate == 0.001){
  $mut_rate = $mut_rate_i;
}
  print "\n loop $loop \n ";
  
  print "\nstarting dynamics\n";
  &dinamic;

  print "\nstarting selection \n";
  &selection;
  $salvados = $new_i - 1;
  
  print "\nstarting initialization \n";
  &init_cicle;
  print "\finidhing initialization \n";
}
&solution;




sub file_handler{
  $output ="output.txt";            #opening output file
  open (OUTPUT,">$output") || die "ERROR: cannot open file $output\n";
  $sol ="solucion.txt";            #open result file
  open (SOL,">$sol") || die "ERROR: cannot open file $solucion\n";
  $e10 ="E10.txt";            #opening last network file
  open (E10,">$e10") || die "ERROR: cannot open file $e10\n";
  $rules ="rules.txt";            #opening rules file
  open (RULES,">$rules") || die "ERROR: cannot open file $rules\n";
  @file_lines = <>;
  $i=0;
  foreach (@file_lines){
    if($i==0){
      @splits = split(/,/, $_);
      $num_nodes = $splits[0];
      $num_ind = $splits[1];
      $num_states = $splits[2];
      $num_elem = $num_nodes*$num_nodes;
      $num_rules = $num_nodes;
      $num_total_elem = $num_elem + $num_rules;
      print "\ntotal number of elements: $num_total_elem\n";

    }elsif ($i==1){

      @data = split(/,/, $_);
      chomp(@data);
    }elsif ($i<=$num_states+1){

      @states = split(/,/, $_);
      @states_ind[$i-1] = [@states];
      chomp(@states_ind);
      for($j=1;$j<=$num_nodes;$j++){
#        $states_ind[2][$j-1] = chomp($states_ind[1][$j-1]);
      }
    }elsif ($_ =~ /Individuos_SA/){

      last;
    }elsif ($i == 4){

      @initial_int = split(/,/, $_);
    } #end if
    ++$i;
  } #end foreach
} #end sub file_handler

#Initializing population from output SA file. Use 'sub init' for GA standalone
sub init_SA{

  #my @population;
  #@indiv = <>;
  print $file_lines[1];
  $x=0;
  $m=1;
  $j=1;
  $k=1;

  foreach(@file_lines){
  chomp ($_);
    print "$x,$m\n";
    if($_ =~ /ENLACES/){
      print "enlaces\n";
      $x=1;

      next;
    }elsif($_ =~ /REGLAS/){
    print "reglas\n";
      $x=2;

      next;
    }
    if($x==1){

      $_ =~ s/\s+/;/g;
      chomp($_);
      @splits = split(/;/, $_);

   # @splits = split(/ /, $splits);
      chomp(@splits);
      $j=$k;
      for($i=1;$i<=$#splits;$i++){

        chomp(@splits[$i]);
        $population[$m][$j] = $splits[$i];

        $j=$k+$num_nodes*$i;
      }
      $k++;
      #@population[$m] = [@splits];
      #$m++;
    }elsif($x==2){

      $j=$num_nodes*$num_nodes+1;
      $_ =~ s/\s+/;/g;
      @splits = split(/;/, $_);
      chomp(@splits);
      for($i=1;$i<=$#splits;$i++){

        chomp(@splits[$i]);
        $population[$m][$j] = $splits[$i];

        $j++;
      }
      #@population[$m] = [@splits];
      $m++;
      $x=0;
      $j=1;
      $k=1;

    }else{
      next;
    }


  }
  print "p=$#population\n";

}


#Initialize population with first individual interactions in file and fill the rest 
#with permutations on that one
sub init{
print @initial_int;
print "\n";
  $p = 0;
  $q = 1;
  for($i=1; $i<=$num_ind; $i++){
    for($j=1; $j<=$num_elem; $j++){
      if($p<$num_elem){
        $population[$i][$j] = $initial_int[$p];

      }else{
        if($p==$num_elem){$p=0;}
        $population[$i][$j] = $initial_int[$p];

      } #end if
      ++$p;
    } #end for $j
    
    # Filling the end of each individual with the rule set that defines their dynamics
    # the rules are initialized randomly
    $start_rules = $num_elem+1;
    $end_rules = $num_elem+$num_nodes;

    for($j=$start_rules; $j<=$end_rules; $j++){
      $population[$i][$j] = 1;
      $random_number = rand();
      if($random_number <= $init_mut_rate2){
        $population[$i][$j] = int(rand(3))+2;
      }

    }
    print "\n";
    ++$q;
    $p=$q;
  } #end for $i
}

sub dinamic{

  $l = 1;




# For all individuals
  for($i2=1;$i2<=$num_ind;$i2++){
  # initialize starting state
    for($i=1;$i<=$num_nodes;$i++){
      $state_e9[$i] = $states_ind[1][$i-1];

    }
  
  
# Five loops may ensure steady-state
    for($m=1;$m<=5;$m++){
    
      for($i=1;$i<=$num_nodes;$i++){
        $cont_total = 0;
        $cont_posit = 0;
        $cont_neg = 0;
        $cont_pos = 0;
        my $pos = ();
        my $val_pop = ();
        my $val_e9 = ();
        $cont_rules = 1;
    
        for($j=$i;$j<=$num_elem;$j+=$num_nodes){
          if($population[$i2][$j] == 1 || $population[$i2][$j] == -1){
            if($population[$i2][$j] == 1){
              ++$cont_posit;
            }
            if($population[$i2][$j] == -1){
              ++$cont_neg;
            }
                 
            $pos[$l] = $cont_pos;
            $val_pop[$l] = $population[$i2][$j];
            $val_e9[$l] = $state_e9[$l];
          
            ++$l;
            ++$cont_total;
          }
          ++$cont_pos;
        }
        $pos_rule = $num_elem + $i;
        $rule_value = $population[$i2][$pos_rule];

		#Select rules
        switch ($rule_value){
          case 1 { #Majority rule
            if($cont_posit > $cont_neg){
              $state_e9[$i] = 1;
            }elsif($cont_posit < $cont_neg){
              $state_e9[$i] = 0;
            }

          }
          case 2 { #Absolute repressor rule
            if($cont_neg == 1 && $cont_total == 1){
              $state_e9[$i] = 0;
            }

          }
          case 3 { #Co-activators rule
           if($cont_posit == 2 && $cont_total == 2){
             $state_e9[$i] = 1;
           }
   #        print " entra en 3\n";
          }
          case 4 { #Co-repressors rule
            if($cont_neg == 2 && $cont_total == 2){
              $state_e9[$i] = 0;
            }

          }
          else{}
        }
      
      
     


        ++$cont_rules;
      } #end for $i   
    } #end for $m
    &fitness($i2);
  } #end for $i2
  &roulette;
} #end sub dinamic

#Individuals fitness
sub fitness{
  my @int_fit = ();
  $sum_fit = 0;
  $int_fit[0] = 1;
  
  $cont_e10 = 0;
  $cont_int = 0;
  $cont_edges = 0;
  $i = $_[0];

# Measure distance between virtual final and control final state
  for($n=1;$n<=$num_nodes;++$n){
    if($state_e9[$n] != $states_ind[2][$n-1]){
      ++$cont_e10;
    }
  }
  $cont_e10_print = $cont_e10;
  $cont_e10 = $cont_e10*$cont_e10;

# Counting links and measuring distances
  for($m=1;$m<=$num_elem;++$m){
    if($population[$i][$m] == 1 || $population[$i][$m] == -1){
      ++$cont_edges;
    }
    if($initial_int[$m-1] != $population[$i][$m]){
      ++$cont_int;
    }
  }

  
# Individual is rejected if number of links is out of thresholds
  if($cont_edges < 0.1*$num_nodes || $cont_edges > $top){
    $phi[$i] = $num_elem;
  }else{
    $phi[$i] = $lambda*$cont_e10 + (1 - $lambda)*$cont_int;
  }

  $normphi[$i] = $phi[$i]/$num_elem;
  $phi[0] = $num_elem;
  if($phi[$i]<$phi[$i-1]){
    $win2 = $win1;
    $win1 = $normphi[$i];
    $win1_med = $win1_med + $win1;
  }
  $normphi_med = $normphi_med + $normphi[$i];
  if($i==$num_ind){
    $win1_med = $win1_med/$num_ind;
    $normphi_med = $normphi_med/$num_ind;
    print OUTPUT "$loop\t$win1\t$win2\t$win1_med\t$normphi_med\t";
    $win1 = 0;
    $win2 = 0;
    $win1_med = 0;
    $normphi_med = 0;
  }
  
  $nphi[$i] = 1-$phi[$i]/$num_elem;


  if($nphi[$i] >= $win_value){
    if($nphi[$i] > $win_value2){
    
      $mut_rate = $mut_rate_i;
      $sin_new = 0;
      $sin_new2 = 0;
      
      $win_second_value = $win_value2;
      $win_value2 = $nphi[$i];
      $win_value2_phi = $phi[$i]/$num_elem;
      $win_value = $win_value2;
      $win_pos2 = $loop;
      $cont_e10_print2 = $cont_e10_print;
      $cont_int_win = $cont_int;
      for($n3=1;$n3<=$num_nodes;++$n3){
        $state_e10[$n3] = $state_e9[$n3];
      }
      for($m3=1;$m3<=$num_nodes;++$m3){
        $solution2[$m3] = $population[$i][$m3];
      }
      print E10 "winner: loop $loop pos $i value $win_value2 $win_pos2 Dist_nodos=$cont_e10_print2 Dist_int=$cont_int_win\n";
      print E10 "Final state\n";
      print E10 @state_e10;
      print E10 "\n";
      print E10 "Links\n";
      for($m4=1;$m4<=$num_elem;$m4++){
        print E10 "$population[$i][$m4],";
      }
      print E10 "\n";
      print E10 "Rules\n";
      for($m4=$num_elem+1;$m4<=$num_total_elem;$m4++){
        print E10 "$population[$i][$m4],";
        print RULES "$population[$i][$m4],";
      }
      print E10 "\n";
      print E10 "\n";
      print RULES "\n";
      print "\n";
    } #if $nphi value2
    $win_value = $nphi[$i];
    $win_pos = $i;
    $win_loop = $loop;

   
  } #if $nphi value
  if($i==$num_ind){
    print OUTPUT "$win_value2_phi\n";
  }


  if($loop == $num_loop){
    for($m2=1;$m2<=$num_elem;$m2++){
      $solution[$m2] = $population[$win_pos][$m2];
    }
    if($i==$num_ind){
      for($m2=$num_elem+1;$m2<=$num_total_elem;$m2++){
        print RULES "$population[$win_pos][$m2]";
      }
    }
  }
  
} #end sub fitness


# Roulette method to choose individuals for crossover
sub roulette{

  $roulette[0] = 0;
    
  for($n2=1;$n2<=$num_ind;$n2++){
    $sum_fit = $sum_fit + $nphi[$n2];
  }
    
  for($n2=1;$n2<=$num_ind;$n2++){
    $roulette[$n2] = $roulette[$n2-1] + $nphi[$n2]/$sum_fit;

  }
  
  $sum_fit = 0;

}


# Only best individuals are selected
sub selection{
  print "winpos=$win_pos\n";
# First individual is the best one
  for($j=1;$j<=$num_total_elem;$j++){

    $new_population[1][$j] = $population[$win_pos][$j];

  }
  $new_i = 2;

  for($i=2;$i<=$num_ind+1;$i++){

    $num_rand = rand();
    $num_rand2 = rand();
 
      for($i2=$num_ind;$i2>=1;$i2--){
        if($roulette[$i2] < $num_rand && $num_rand != 2){

          $child1 = $i2;
          $num_rand = 2;
        }
    
        if($roulette[$i2] < $num_rand2 && $num_rand2 != 2){

          $child2 = $i2;
          $num_rand2 = 2;
        }
    
        if($num_rand == 2 && $num_rand == 2){
 
        }
      } #end for $i2

      $valid = 0;
      $cont_int1 = 0;
      $cont_int2 = 0;
      $valid1 = 0;
      $valid2 = 0;
  
      while($valid != 1){
  #Crossover point is randomly selected
        $rand_num3 = int(rand($num_elem))+1;
        for($j=1;$j<=$num_elem;$j++){
 
          if($j<=$rand_num3){
            if($valid1 != 1){
              $new_population[$i][$j] = $population[$child1][$j];
              &mutation($i,$j);
            }
            if($valid2 != 1){        
              $new_population[$i+1][$j] = $population[$child2][$j];
              &mutation($i+1,$j);
            }
          }else{
            if($valid1 != 1){
              $new_population[$i][$j] = $population[$child2][$j];
              &mutation($i,$j);
            }
            if($valid2 != 1){        
              $new_population[$i+1][$j] = $population[$child1][$j];
              &mutation($i+1,$j);
            }
          } #end if $j
# Counting new individual population
          if($new_population[$i][$j] != 0 && $valid1 != 1){
            ++$cont_int1;
          }
          if($new_population[$i+1][$j] != 0 && $valid2 != 1){
            ++$cont_int2;
          }
        } #end for $j
        ++$cont_while;

# Individual is rejected if number of links is out of thresholds
        if(($cont_int1>0.1*$num_nodes && $cont_int1<=$top) && ($cont_int2>0.1*$num_nodes && $cont_int2<=$top)){
          $valid = 1;
          $cont_int1 = 0;
          $cont_int2 = 0;
          $valid1 = 0;
          $valid2 = 0;
        }elsif($cont_int1>0.1*$num_nodes && $cont_int1<=$top){
          $valid1 = 1;
          $cont_int2 = 0;
        }elsif($cont_int2>0.1*$num_nodes && $cont_int2<=$top){
          $valid2 = 1;
          $cont_int1 = 0;
        }else{
          $cont_int1 = 0;
          $cont_int2 = 0;
          $valid1 = 0;
          $valid2 = 0;
        }

      } #end while
      $cont_while = 0;
# ++$new_i;
# ++$new_i;

    
    # Rule crossover
    $rand_num4 = int(rand($num_nodes))+1;
    for($j=$num_elem+1;$j<=$num_total_elem;$j++){
      if($j<=$rand_num3){
        $new_population[$i][$j] = $population[$child1][$j];
        &mutation2($i,$j);
        $new_population[$i+1][$j] = $population[$child2][$j];
        &mutation2($i+1,$j);
      }else{
        $new_population[$i][$j] = $population[$child2][$j];
        &mutation2($i,$j);
        $new_population[$i+1][$j] = $population[$child1][$j];
        &mutation2($i+1,$j);
      } # end if $j
    } # end for $j = $num_elem+1
    
    ++$i;
  } #end for $i


} # end sub selection


#Mutation: If link, mutation among values -1, 0 and 1; if node mutation between 0 and 1
sub mutation{
local($i2, $j2);
$i2 = $_[0];
$j2 = $_[1];
  $rand_num = rand();
  if ($rand_num <= $mut_rate){

  
$rand_num2 = int(rand(2));
    if ($new_population[$i2][$j2] == -1){
      $new_population[$i2][$j2] = $rand_num2;
    }elsif($new_population[$i2][$j2] == 1){
      if($rand_num2 == 1){
        $new_population[$i2][$j2] = -1;
      }else{
        $new_population[$i2][$j2] = 0;
      }
    }else{
      if($rand_num2 == 0){
        $new_population[$i2][$j2] = -1;
      }else{
        $new_population[$i2][$j2] = 1;
      }
  }
  }
} # end sub mutation

sub mutation2{
  local($i2, $j2);
  $i2 = $_[0];
  $j2 = $_[1];
    $rand_num = rand();
    if ($rand_num <= $mut_rate2){
      $population[$i2][$j2] = int(rand(4))+1;
    }
} # end sub mutation2


#Load initial population
sub init_cicle{
  #Erasing initial population
  my %saved_ind = ();
  my @sorted = ();
  my @int_fit = ();

  for ($i=1; $i<=$num_ind+2; $i++){
    shift @population;
    shift @int_fit;
    shift @int_fit2;
    shift @sorted;
    shift @roulette;
    shift @phi;
    shift @nphi;
  }
   
  #Re-initializing population for new cycle
  for ($i=1; $i<=$num_ind; $i++){
    for($j=1;$j<=$num_total_elem;$j++){
      $population[$i][$j] = $new_population[$i][$j];
    }
  }
print "\n";
for($j=1;$j<=$num_elem;$j++){
 
    }

  #Erase copied population
  for ($i=1; $i<=$num_ind+2; $i++){
    shift @new_population;
  }
}

#Print result
sub solution{
  for ($j=1; $j<=$num_nodes; $j++){
    for ($l=1; $l<=$num_nodes; $l++){
      $k = ($j-1)*$num_nodes+$l;
      if($solution[$k] == 1 || $solution[$k] == -1){
        print SOL "$data[$j-1] $data[$l-1] $solution[$k] $k\n";
      }
    }
  }
}
