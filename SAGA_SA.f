c Simulated annealing to optimize a gene network evolution of 42 genes.
c It generates 100 individuals
       parameter(ng=42)
       dimension miex(ng,ng),ndfe(ng),ndie(ng),ndfs(ng),ndis
     * (ng),mis(ng,ng),nr(ng),nrmejor(ng),ndismejor(ng),ndfsmejor(ng),
     *  mismejor(ng,ng)
c miex gives the configuration for final control network links(input), 
c ndfe(ng) gives final control nodes(E10),
c ndie(ng) gives initial control nodes(E9),
c ndfs(ng) gives simulated final nodes,
c ndis(ng) gives initial simulated nodes,
c mfs(ng,ng) gives simulated final links(input),
c mis(ng,ng) gives initial simulated links(input),
c nr(ng) gives applied rules

c Reading data from initial file
       nenlaces=ng*ng
c iu is the writing unit
       read*,iu
c idum is the random numbers seed,
c rlanda is the weight between nodes and links in the fitness function,
c t0 is the initial temperature
c gamma is gamma.
c mtopetemp is the number of MonteCarlo runs at each given temperature,
c tmin is the minimum stop temperature, 
c mtopeenlaces is the maximum number of links that allows a configuration, 
c maxact is the number of updates performed in each dynamical stage
c nind is the population size

       read*,idum     
       read*,t0,tmin,gamma       
       read*,mtopetemp,mtopeenlaces,maxact
         
c reading initial and final control nodes
       read(5,1000)ndie
       read(5,1000)ndfe
1000   format(42i2)
c reading control links(input)
       do i=1,ng
          read(5,1010)(miex(i,j),j=1,ng)
       enddo
1010   format(42i3)
       nind=0
400    nind=nind+1
c creating a population of 100 individuals
       if(nind.le.100)then
c generating the initial configuration
       call configini1(ng,mtopeenlaces,idum,ndfs,mis,nr)
       do i=1,ng
          ndis(i)=ndie(i)
       enddo
c calculating the initial configuration energy and the initial temperature is given
       t=t0
       call energia(nenergia1a,nenergia2a,ng,ndis,
     *  ndfs,ndie,ndfe,mis,miex)
       emejor=0.9*nenergia1a+0.1*nenergia2a
       nenergia1mejor=nenergia1a
       nenergia2mejor=nenergia2a

c initializing process
100    call smc(ng,ndis,ndie,ndfs,ndfe,mis,miex,idum,nr,t,
     *  nenlaces,mtopeenlaces,mtopetemp,nenergia1a,nenergia2a,nenergia1,
     *  nenergia2,maxact,nexits)
       e1=0.9*nenergia1+0.1*nenergia2
       if((e1-emejor).lt.0)then
          do i=1,ng
             nrmejor(i)=nr(i)
             ndismejor(i)=ndis(i)
             ndfsmejor(i)=ndfs(i)
          enddo
          do i=1,ng
             do j=1,ng
                mismejor(i,j)=mis(i,j)
             enddo
          enddo
          emejor=e1
          nenergia1mejor=nenergia1
          nenergia2mejor=nenergia2
       endif
          nexits=0
          if(t.gt.tmin)then
             t=gamma*t
             goto 100
          endif

       write(iu,*)"NIND,NENERGIA1,NENERGIA2   ",NIND,NENERGIA1,NENERGIA2
        write(iu,*)"ENLACES  "
        do i=1,ng
          write(iu,2010)(mismejor(i,j),j=1,ng)
        enddo
        write(iu,*)"REGLAS  "
        write(iu,2000)nrmejor
        write(iu,*)"   "
2000	format(42i2)
2010    format(42i3)  
       goto 400
       endif
       stop
       end
       
       subroutine configini1(ng,mtopeenlaces,idum,ndfs,mis,nr)
c this subroutine gives the initial links and rules configuration
c with a maximum number of links given by mtopeenlaces. Values +1 and -1 are
c chosen with the same probability. Rules values are 1, 2, 3 and 4, and are given with
c equal probability.
       dimension ndfs(ng),mis(ng,ng),nr(ng),ntotalii(ng)
       do i=1,ng
          r=ran1(idum)
          if(r.le.0.5)then
             ndfs(i)=1
          else
             ndfs(i)=0
          endif
       enddo
       do i=1,ng
          do j=1,ng
             mis(i,j)=0
          enddo
       enddo
       ncontenl=0
       ntopeenlaces=int(0.75*mtopeenlaces)
100    if (ncontenl.lt.ntopeenlaces)then
          r=ran1(idum)
          ri=ran1(idum)
          rj=ran1(idum)
          ii=int(ri*ng)+1
          jj=int(rj*ng)+1
          if(r.le.0.83) then
             mis(ii,jj)=1
             ncontenl=ncontenl+1
             goto 100
          else
             mis(ii,jj)=-1
             ncontenl=ncontenl+1
             goto 100
          endif
       endif
       do i=1,ng
          ntotalii(i)=0
          do j=1,ng
             ntotalii(i)=ntotalii(i)+iabs(mis(i,j))
          enddo
       enddo
       do i=1,ng
          if(ntotalii(i).eq.0)then
             r=ran1(idum)
             kk=int(r*42)+1
             mis(i,kk)=1
          endif
       enddo
       do i=1,ng
          r=ran1(idum)
          if(r.le.0.25)then
             nr(i)=1
          elseif(r.le.0.5)then
             nr(i)=2
          elseif(r.le.0.75)then
             nr(i)=3
          else
             nr(i)=4
          endif
       enddo
       return
       end
       
       subroutine energia(nenergia1,nenergia2,ng,nd1s,nd2s,nd1e,
     *  nd2e,menl1,menl2)
c this subroutine calculates the energy (objective function) value by calculating 
c simulated with control nodes and links, with a weight rlanda for the terms
c nenergia1(nodes) and nenenergia2(enlaces)
       dimension nd1s(ng),nd2s(ng),nd1e(ng),nd2e(ng),menl1(ng,ng),
     * menl2(ng,ng)
       nenergia1=0
       do i=1,ng
          if(nd1s(i).ne.nd1e(i))then
             nenergia1=nenergia1+1
          endif
       enddo
       do i=1,ng
          if(nd2s(i).ne.nd2e(i))then
             nenergia1=nenergia1+1
          endif
       enddo
       nenergia2=0
       do i=1,ng
          do j=1,ng
             if(menl1(i,j).ne.menl2(i,j))then
                nenergia2=nenergia2+1
             endif
          enddo
       enddo
       return
       end
          
       subroutine smc(ng,ndis,ndie,ndfs,ndfe,mis,miex,idum,nr,t,
     * nenlaces,mtopeenlaces,mtopetemp,nenergia1a,nenergia2a,nenergia1,
     * nenergia2,maxact,nexits)
       dimension ndfs(ng),ndfe(ng),mis(ng,ng),miex(ng,ng),nr(ng),
     * ntotalii(ng),ndis(ng),ndie(ng)
       do i=1,ng
          ntotalii(i)=0
          do j=1,ng
             ntotalii(i)=ntotalii(i)+iabs(mis(i,j))
          enddo
       enddo
       ncontenl=0
       do i=1,ng
          ncontenl=ncontenl+ntotalii(i)
       enddo
       ncontt=0
190    nmcarlo=0
       nexits=0
200    r=ran1(idum)
       kk=int(r*ng)+1
210    r=ran1(idum)
       ii=int(r*ng)+1
       r=ran1(idum)
       jj=int(r*ng)+1
       r=ran1(idum)
       if(r.le.0.015)then
          misr=mis(ii,jj)
          mis(ii,jj)=-1
       elseif(r.le.0.089)then
          misr=mis(ii,jj)
          mis(ii,jj)=1
       else
          misr=mis(ii,jj)
          mis(ii,jj)=0
       endif
       ntotalii(ii)=ntotalii(ii)+iabs(mis(ii,jj))-iabs(misr)
       if(ntotalii(ii).eq.0)then
          ntotalii(ii)=ntotalii(ii)-iabs(mis(ii,jj))+iabs(misr)
          mis(ii,jj)=misr
          goto 210
       endif
       ncontenl=ncontenl+iabs(mis(ii,jj))-iabs(misr)
       if(ncontenl.gt.mtopeenlaces)then
          ncontenl=ncontenl-iabs(mis(ii,jj))+iabs(misr)
          mis(ii,jj)=misr
          goto 210
       endif
       r=ran1(idum)
       if(r.le.0.25)then
          nrr=nr(kk)
          nr(kk)=1
       elseif(r.le.0.5)then
          nrr=nr(kk)
          nr(kk)=2
       elseif(r.le.0.75)then
          nrr=nr(kk)
          nr(kk)=3
       else
          nrr=nr(kk)
          nr(kk)=4
       endif
       call evol(ng,maxact,ndfs,ndis,mis,nr)
       call energia(nenergia1,nenergia2,ng,ndis,ndfs,ndie,ndfe,mis,
     *  miex)
       delta1=nenergia1-nenergia1a
       delta2=nenergia2-nenergia2a
       if(delta1.lt.0)then
          nenergia1a=nenergia1
          nenergia2a=nenergia2
          nexits=nexits+1
       elseif(delta1.gt.0)then
          pb1=exp(-delta1/t)
          r=ran1(idum)
          if(r.le.pb1)then
             nenergia1a=nenergia1
             nenergia2a=nenergia2
             nexits=nexits+1
          else
             mis(ii,jj)=misr
             nr(kk)=nrr
          endif
       else
          if(delta2.lt.0)then
             nenergia1a=nenergia1
             nenergia2a=nenergia2
             nexits=nexits+1
          elseif(delta2.gt.0)then
             pb2=exp(-delta2/t)
             r=ran1(idum)
             if(r.le.pb2)then
                nenergia1a=nenergia1
                nenergia2a=nenergia2
                nexits=nexits+1
             else
                mis(ii,jj)=misr
                nr(kk)=nrr
             endif
          else
             mis(ii,jj)=misr
             nr(kk)=nrr
          endif
       endif
       nmcarlo=nmcarlo+1
       if(nmcarlo.lt.nenlaces)then
          goto 200
       endif
       ncontt=ncontt+1
       if(ncontt.lt.mtopetemp)then
          ncontt=ncontt+1
          goto 190
       endif
       return
       end
       
       subroutine evol(ng,maxact,ndfs,ndis,mis,nr)
       dimension ndfs(ng),ndis(ng),mis(ng,ng),nr(ng),ndiso(ng)
       ncontact=0
       do i=1,ng
          ndiso(i)=ndis(i)
       enddo
90        do i=1,ng
             call nact(ndfs(i),ng,nr,mis,ndiso,i)
          enddo
          ndiferencia=0
          do i=1,ng
             if(ndfs(i).ne.ndiso(i))then
                ndiferencia=ndiferencia+1
             endif
          enddo
          if(ndiferencia.eq.0)then
             goto 100
          endif
       do i=1,ng
          ndiso(i)=ndfs(i)
       enddo
       ncontact=ncontact+1
       if(ncontact.lt.maxact)then
          goto 90
       endif
100    return
       end

       
       subroutine nact(ndfsi,ng,nr,mis,ndiso,i)
       dimension nr(ng),mis(ng,ng),ndiso(ng)
       if(nr(i).eq.1)then
          nsuma=0
          do j=1,ng
             nsuma=nsuma+mis(i,j)*ndiso(j)
          enddo
          if(nsuma.lt.0)then
             ndfsi=0
          elseif(nsuma.gt.0)then
             ndfsi=1
          endif
          goto 300
       elseif(nr(i).eq.2)then
          do j=1,ng
             if(mis(i,j).eq.-1)then
                ndfsi=0
                goto 300
             endif
          enddo
       elseif(nr(i).eq.3)then
          nsuma=0
          do j=1,ng
             if(mis(i,j).eq.1)then
                nsuma=nsuma+1
                if(nsuma.eq.2)then
                   ndfsi=1
                   goto 300
                endif
             endif
          enddo
       else
          nsuma=0
          do j=1,ng
             if(mis(i,j).eq.-1)then
                nsuma=nsuma+1
                if(nsuma.eq.2)then
                   ndfsi=-1
                   goto 300
                endif
             endif
          enddo
       endif
300    return
       end
       
        FUNCTION ran1(idum)
        INTEGER idum,IA,IM,IQ,IR,NTAB,NDIV
        REAL ran1,AM,EPS,RNMX
        PARAMETER (IA=16807,IM=2147483647,AM=1./IM,IQ=127773,IR=2836,
     *    NTAB=32,NDIV=1+(IM-1)/NTAB,EPS=1.2e-7,RNMX=1.-EPS)
        INTEGER j,k,iv(NTAB),iy
        SAVE iv,iy
        DATA iv /NTAB*0/, iy /0/
        if (idum.le.0.or.iy.eq.0) then
          idum=max(-idum,1)
          do 11 j=NTAB+8,1,-1
              k=idum/IQ
              idum=IA*(idum-k*IQ)-IR*k
              if (idum.lt.0) idum=idum+IM
              if (j.le.NTAB) iv(j)=idum
11        continue
          iy=iv(1)
        endif
        k=idum/IQ
        idum=IA*(idum-k*IQ)-IR*k
        if (idum.lt.0) idum=idum+IM
        j=1+iy/NDIV
        iy=iv(j)
        iv(j)=idum
        ran1=min(AM*iy,RNMX)
        return
        END



       



          
       

